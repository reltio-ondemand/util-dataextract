package com.reltio.util;

import org.junit.Test;
import org.testng.Assert;

public class PropertyUtilTest {

    @Test
    public void getMutualExclusiveProps() {

        Assert.assertEquals(
                PropertyUtil.getMutualExclusiveProps().toString(),
                "{[USERNAME, PASSWORD]=[CLIENT_CREDENTIALS]}");

    }
}