package com.reltio.util.extract.denormalized.service;
import com.reltio.relations.RelationExtractByDBScan;
import com.reltio.relations.RelationExtractByDBScanSingleThread;
import com.reltio.extract.denormalized.service.AttributeExtractReport;
 import com.reltio.extract.denormalized.service.XRefExtractReport;

import org.junit.Ignore;
import org.junit.Test;

public class AttributeExtractReportTest {


    @Test
    @Ignore
    public void smartExportTestOvExtract() throws Exception {
        String[] filePath = {"src/test/resources/smartExport/OV_EXTRACT.properties"};
        AttributeExtractReport.main(filePath);
    }

    @Test
    @Ignore
    public void smartExportTestXrefExtract() throws Exception {
        String[] filePath = {"src/test/resources/smartExport/XREF_EXTRACT.properties"};
        XRefExtractReport.main(filePath);
    }

    @Test
    @Ignore
    public void dbScanExport() throws Exception {
        String[] filePath = {"src/test/resources/dbScanExport/OV_EXTRACT.properties"};
        AttributeExtractReport.main(filePath);
    }


    @Test
    public void rocs102() throws Exception {
        String[] filePath = {"src/test/resources/rocs-102/config1.properties"};
        AttributeExtractReport.main(filePath);
    }

    @Test
    public void rocs103() throws Exception {
        String[] filePath = {"src/test/resources/rocs-103/RelationExtract.properties"};
        RelationExtractByDBScan.main(filePath);
    }
    
    @Test
    public void removedThreads() throws Exception {
        String[] filePath = {"src/test/resources/rocs-103/RelationExtract.properties"};
        RelationExtractByDBScanSingleThread.main(filePath);
    }
}