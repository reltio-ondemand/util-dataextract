package com.reltio.extract.denormalized.service;

import com.reltio.cst.service.ReltioAPIService;
import com.reltio.cst.util.Util;
import com.reltio.extract.domain.Crosswalk;
import com.reltio.extract.domain.ExtractProperties;
import com.reltio.extract.domain.InputAttribute;
import com.reltio.extract.domain.ReltioObject;
import com.reltio.extract.domain.XrefInputAttribute;
import com.reltio.extract.service.builder.EntityReaderBuilder;
import com.reltio.extract.service.readers.EntityReader;
import com.reltio.extract.util.ExtractServiceUtil;
import com.reltio.file.ReltioCSVFileWriter;
import com.reltio.file.ReltioFileWriter;
import com.reltio.file.ReltioFlatFileWriter;
import com.reltio.util.PropertyUtil;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.*;
import java.util.concurrent.*;

import static com.reltio.cst.util.Util.waitForTasksReady;
import static com.reltio.extract.ExtractConstants.ENCODING;

/**
 * This is the main class for generating the Extract Report
 *
 * @author Ganesh
 */

public class XRefExtractReport extends ExtractReport {

    // Default Reltio XREF attributes in the File
    private static final String[] DEFAULT_ATTRIBUTES = {"ReltioURI", "SourceSystem", "SourceId", "SourceTable", "CreateDate", "UpdateDate", "DeleteDate"};
    private static final String OV_ONLY = "OV_ONLY";

    /*
     * private static final String[] DefaultAttributes = { "ReltioURI",
     * "SourceSystem", "SourceId", "CreateDate", "UpdateDate", "DeleteDate" };
     */
    public static void main(String[] args) throws Exception {

        System.out.println("Process Started");
        final int MAX_QUEUE_SIZE_MULTIPLICATION = 10;

        if (args.length == 0) {
            System.out.println("Please pass file path as argument!!!");
            System.exit(0);
        }

        long programStartTime = System.currentTimeMillis();

        // Encrypting Password
        Properties properties = Util.getProperties(args[0], "PASSWORD", "CLIENT_CREDENTIALS");

        List<String> missingKeys = Util.listMissingProperties(properties,
                Arrays.asList("ENVIRONMENT_URL", "TENANT_ID", "AUTH_URL", "ENTITY_TYPE", "USERNAME", "EXTRACT_TYPE"),
                PropertyUtil.getMutualExclusiveProps());


        if (Util.isEmpty(missingKeys)) {
            
            Set<String> keys = properties.stringPropertyNames();
            System.out.println("Extract starts with following properties.");

            keys.stream().filter(key -> !"PASSWORD".equalsIgnoreCase(key)).filter(key -> !"CLIENT_CREDENTIALS".equalsIgnoreCase(key)).
                    forEach(key -> System.out.println(key + " : " + properties.getProperty(key)));
        } else {
            System.out.println("Properties are missing \n" + String.join("\n", missingKeys));
            System.exit(0);
        }

        // Proxy setup
        if (!Util.isEmpty(properties.getProperty("HTTP_PROXY_HOST"))
                && !Util.isEmpty(properties.getProperty("HTTP_PROXY_PORT"))) {
            Util.setHttpProxy(properties);
        }

        // READ the Properties values

        final ExtractProperties extractProperties = new ExtractProperties(properties);

        ReltioAPIService reltioAPIService = Util.getReltioService(properties);

        final Map<String, InputAttribute> attributes = new LinkedHashMap<>();

        int count = 0;

        // Read Crosswalk Value Attributes
        if (extractProperties.getXrefAttrFilePath() != null && !extractProperties.getXrefAttrFilePath().isEmpty()
                && !extractProperties.getXrefAttrFilePath().equalsIgnoreCase("null")) {
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(new FileInputStream(extractProperties.getXrefAttrFilePath()), ENCODING));
            ExtractServiceUtil.createAttributeMapFromProperties(reader, attributes);
            Util.close(reader);
        }

        final ReltioFileWriter reltioFileWriter;

        // Output File
        // check whether its CSV out FILE or Flat File
        if (extractProperties.getFileFormat().equalsIgnoreCase("CSV")) {
            reltioFileWriter = new ReltioCSVFileWriter(extractProperties.getOutputFilePath(), ENCODING);
        } else if (extractProperties.getFileDelimiter() != null) {
            // provided file Delimiter
            reltioFileWriter = new ReltioFlatFileWriter(extractProperties.getOutputFilePath(), ENCODING,
                    extractProperties.getFileDelimiter());
        } else {
            // Default delimiter pipe
            reltioFileWriter = new ReltioFlatFileWriter(extractProperties.getOutputFilePath(), ENCODING);
        }

        final List<String> headers = new ArrayList<>(Arrays.asList(DEFAULT_ATTRIBUTES));

        // Create Response file header
        for (Map.Entry<String, InputAttribute> attr : attributes.entrySet()) {
            ExtractServiceUtil.createExtractNestedResponseHeader(attr, headers, null, extractProperties.getLookUpAttributes());
        }

        final String[] responseHeader = new String[headers.size()];
        headers.toArray(responseHeader);

        if (extractProperties.getIsHeaderRequired() == null
                || extractProperties.getIsHeaderRequired().equalsIgnoreCase("Yes")) {
            reltioFileWriter.writeToFile(responseHeader);
        }

        /*
         * if (extractProperties.getIsHeaderRequired().equalsIgnoreCase( "Yes")) {
         * scanUrl = extractProperties.getApiUrl() + SCAN_URL +
         * "?select=uri,crosswalks"; if (attributes == null || attributes.isEmpty() &&
         * extractProperties.getIsHeaderRequired().equalsIgnoreCase( }
         */

        ExecutorService executorService = Executors
                .newFixedThreadPool(extractProperties.getThreadCount());

        boolean eof = false;

        List<Future<Long>> futures = new ArrayList<>();

        properties.put("SELECT", "uri,crosswalks,attributes." + String.join(",attributes.", attributes.keySet()));
        properties.put("TYPE_OF_DATA", "entities");
        properties.put(OV_ONLY, "false");

        EntityReader reader = EntityReaderBuilder.build(properties, reltioAPIService);

        while (!eof) {

            for (int threadNum = 0; threadNum < extractProperties.getThreadCount()
                    * MAX_QUEUE_SIZE_MULTIPLICATION; threadNum++) {

                // Doing the DBScan API Call here
                // Convert the string the java object
                List<ReltioObject> objects = reader.getReltioObjects();

                if (objects != null && objects.size() > 0) {
                    count += objects.size();
                    final List<ReltioObject> objectsToProcess = objects;

                    futures.add(executorService.submit(() -> {

                        long requestExecutionTime = 0L;

                        try {

                            long startTime = System.currentTimeMillis();
                            // Getting the required response for XREF output
                            List<Map<String, String>> responseMap = getXrefResponse(objectsToProcess, attributes);

                            List<String[]> finalResponse = new ArrayList<>();

                            responseMap.forEach(resMap -> finalResponse.add(
                                    objectArrayToStringArray(filterMapToObjectArray(resMap, responseHeader))));

                            reltioFileWriter.writeToFile(finalResponse);

                            requestExecutionTime = System.currentTimeMillis() - startTime;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        return requestExecutionTime;
                    }));

                    System.out.println("Scanned records count >>> " + count);

                } else {
                    eof = true;
                    break;
                }
            }

            waitForTasksReady(futures, extractProperties.getThreadCount() * (MAX_QUEUE_SIZE_MULTIPLICATION / 2));
        }

        waitForTasksReady(futures, 0);
        executorService.shutdown();
        Util.close(reltioFileWriter);
        reader.destroy();

        System.out.println("Completed..... ");
        long finalTime = System.currentTimeMillis() - programStartTime;
        System.out.println("[Performance]:  Total processing time : " + (finalTime / 1000) + "  Seconds");
    }

    /**
     * This method will process the List of object returned in the Scan response and
     * extract only required values and put the details in the Map format
     *
     * @param objects
     * @return
     */
    public static List<Map<String, String>> getXrefResponse(List<ReltioObject> objects,
                                                            Map<String, InputAttribute> attributes) {

        List<Map<String, String>> finalResponse = new ArrayList<>();

        Map<String, List<XrefInputAttribute>> attrUriMap = null;

        for (ReltioObject reltioObject : objects) {
            Map<String, String> res = new HashMap<>();
            res.put("ReltioURI", reltioObject.uri);
            attrUriMap = null;

            if (!attributes.isEmpty()) {
                attrUriMap = new LinkedHashMap<>();
                for (Map.Entry<String, InputAttribute> attr : attributes.entrySet()) {
                    List<Object> attributeData = reltioObject.attributes.get(attr.getKey());
                    ExtractServiceUtil.createUriValuesData(attr, attributeData, attrUriMap);
                }
            }

            // Processing XREF Value
            for (Crosswalk crosswalk : reltioObject.crosswalks) {

                Map<String, String> crosswalkData = new HashMap<>(res);

                crosswalkData.put("SourceSystem", crosswalk.type.split("/")[2]);
                crosswalkData.put("SourceId", crosswalk.value);
                crosswalkData.put("CreateDate", crosswalk.createDate);
                crosswalkData.put("UpdateDate", crosswalk.updateDate);
                crosswalkData.put("DeleteDate", crosswalk.deleteDate);
                crosswalkData.put("SourceTable", crosswalk.sourceTable);

                if (attrUriMap != null) {
                    for (Map.Entry<String, List<XrefInputAttribute>> inputAttr : attrUriMap.entrySet()) {
                        ExtractServiceUtil.createXrefExtractOutputData(inputAttr, crosswalk, crosswalkData, null);
                    }
                }
                finalResponse.add(crosswalkData);
            }
        }
        return finalResponse;
    }

    /**
     * Converts a Map to an array of objects, adding only those entries whose key is
     * in the nameMapping array.
     *
     * @param values      the Map of values to convert
     * @param nameMapping the keys to extract from the Map (elements in the target
     *                    array will be added in this order)
     * @return the array of Objects
     * @throws NullPointerException if values or nameMapping is null
     */
    public static Object[] filterMapToObjectArray(final Map<String, ?> values, final String[] nameMapping) {

        if (values == null) {
            throw new NullPointerException("values should not be null");
        } else if (nameMapping == null) {
            throw new NullPointerException("nameMapping should not be null");
        }

        final Object[] targetArray = new Object[nameMapping.length];
        int i = 0;

        for (final String name : nameMapping) {
            targetArray[i++] = values.get(name);
        }

        return targetArray;
    }

    /**
     * Converts an Object array to a String array (null-safe), by calling toString()
     * on each element.
     *
     * @param objectArray the Object array
     * @return the String array, or null if objectArray is null
     */
    public static String[] objectArrayToStringArray(final Object[] objectArray) {

        if (objectArray == null) {
            return null;
        }

        final String[] stringArray = new String[objectArray.length];
        for (int i = 0; i < objectArray.length; i++) {
            stringArray[i] = objectArray[i] != null ? objectArray[i].toString() : null;
        }
        return stringArray;
    }
}
