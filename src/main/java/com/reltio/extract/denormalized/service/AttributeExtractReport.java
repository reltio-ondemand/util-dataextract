package com.reltio.extract.denormalized.service;

import static com.reltio.cst.util.Util.waitForTasksReady;
import static com.reltio.extract.ExtractConstants.ENCODING;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import com.reltio.cst.service.ReltioAPIService;
import com.reltio.cst.util.Util;
import com.reltio.extract.ExtractConstants;
import com.reltio.extract.domain.ExtractProperties;
import com.reltio.extract.domain.InputAttribute;
import com.reltio.extract.domain.ReltioObject;
import com.reltio.extract.service.builder.EntityReaderBuilder;
import com.reltio.extract.service.readers.EntityReader;
import com.reltio.extract.util.ExtractServiceUtil;
import com.reltio.file.ReltioCSVFileWriter;
import com.reltio.file.ReltioFileWriter;
import com.reltio.file.ReltioFlatFileWriter;
import com.reltio.util.PropertyUtil;

/**
 * This is the main class for generating the Extract entities
 *
 * @author Ganesh
 */
public class AttributeExtractReport extends ExtractReport {

    // Default Reltio XREF attributes in the File
    private static final String[] DefaultAttributes = {"ReltioURI"};

    private static final int MAX_QUEUE_SIZE_MULTIPLICATION = 10;

    public static void main(String[] args) throws Exception {

        System.out.println("Process Started");

        if (args.length == 0) {
            System.out.println("Please pass file path as argument!!!");
            System.exit(0);
        }

        long programStartTime = System.currentTimeMillis();

        Properties properties = Util.getProperties(args[0], "PASSWORD", "CLIENT_CREDENTIALS");

        List<String> missingKeys = Util.listMissingProperties(properties,
                Arrays.asList("ENVIRONMENT_URL", "TENANT_ID", "AUTH_URL", "ENTITY_TYPE", "USERNAME", "EXTRACT_TYPE"),
                PropertyUtil.getMutualExclusiveProps());

        if (Util.isEmpty(missingKeys)) {

            Set<String> keys = properties.stringPropertyNames();
            System.out.println("Extract starts with following properties.");

            keys.stream().filter(key -> !"PASSWORD".equalsIgnoreCase(key)).filter(key -> !"CLIENT_CREDENTIALS".equalsIgnoreCase(key)).
                    forEach(key -> System.out.println(key + " : " + properties.getProperty(key)));
        } else {

            System.out.println("Properties are missing \n" + String.join("\n", missingKeys));
            System.exit(0);
        }

        // Proxy setup
        if (!Util.isEmpty(properties.getProperty("HTTP_PROXY_HOST"))
                && !Util.isEmpty(properties.getProperty("HTTP_PROXY_PORT"))) {
            Util.setHttpProxy(properties);
        }

        // READ the Properties values
        final ExtractProperties extractProperties = new ExtractProperties(properties);

        ReltioAPIService service = Util.getReltioService(properties);
        final Map<String, InputAttribute> attributes = new LinkedHashMap<>();

        int count = 0;

        // Read OV Values Attribute
        if (extractProperties.getOvAttrFilePath() != null && !extractProperties.getOvAttrFilePath().isEmpty()
                && !extractProperties.getOvAttrFilePath().equalsIgnoreCase("null")) {

            BufferedReader attributeFileReader = new BufferedReader(
                    new InputStreamReader(new FileInputStream(extractProperties.getOvAttrFilePath()), ENCODING));
            ExtractServiceUtil.createAttributeMapFromProperties(attributeFileReader, attributes);

            Util.close(attributeFileReader);
        }

        final ReltioFileWriter fileWriter;

        if (extractProperties.getFileFormat().equalsIgnoreCase("CSV")) {
            fileWriter = new ReltioCSVFileWriter(extractProperties.getOutputFilePath(), ExtractConstants.ENCODING);
        } else if (extractProperties.getFileDelimiter() != null) {
            // provided file Delimiter
            fileWriter = new ReltioFlatFileWriter(extractProperties.getOutputFilePath(), ExtractConstants.ENCODING,
                    extractProperties.getFileDelimiter());
        } else {
            // Default delimiter pipe
            fileWriter = new ReltioFlatFileWriter(extractProperties.getOutputFilePath(), ExtractConstants.ENCODING);
        }

        final List<String> headers = new ArrayList<>();

        Collections.addAll(headers, DefaultAttributes);

        // Create Response file header

        for (Map.Entry<String, InputAttribute> attributeEntry : attributes.entrySet()) {
            ExtractServiceUtil.createExtractNestedResponseHeader(attributeEntry, headers, null, extractProperties.getLookUpAttributes());
        }

        final String[] responseHeader = new String[headers.size()];
        headers.toArray(responseHeader);

        if ("Yes".equalsIgnoreCase(extractProperties.getIsHeaderRequired())) {
            fileWriter.writeToFile(responseHeader);
        }

        ExecutorService executorService = Executors.newFixedThreadPool(extractProperties.getThreadCount());

        boolean isFinished = false;

        List<Future<Long>> futures = new ArrayList<>();

        properties.put("SELECT", "uri,attributes." + String.join(",attributes.", attributes.keySet()));
        properties.put("TYPE_OF_DATA", "entities");

        EntityReader entityReader = EntityReaderBuilder.build(properties, service);

        while (!isFinished) {

            for (int threadNum = 0; threadNum < extractProperties.getThreadCount()
                    * MAX_QUEUE_SIZE_MULTIPLICATION; threadNum++) {

                List<ReltioObject> objects = entityReader.getReltioObjects();

                if (objects != null && objects.size() > 0) {

                    count += objects.size();

                    final List<ReltioObject> objectsToProcess = objects;

                    futures.add(executorService.submit(() -> {

                        try {

                            List<Map<String, String>> responseMap = getXrefResponse(objectsToProcess, attributes, extractProperties.getLookUpAttributes());

                            List<String[]> finalResponse = responseMap.stream().map(resMap -> objectArrayToStringArray(filterMapToObjectArray(resMap, responseHeader)))
                                    .collect(Collectors.toList());

                            fileWriter.writeToFile(finalResponse);

                        } catch (Exception e) {
                            System.out.println("Failed to process the records " + e);
                        }

                        return System.currentTimeMillis();

                    }));

                    System.out.println("Scanned records count = " + count);

                } else {
                    isFinished = true;
                    break;
                }
            }

            waitForTasksReady(futures, extractProperties.getThreadCount() * (MAX_QUEUE_SIZE_MULTIPLICATION / 2));
        }

        waitForTasksReady(futures, 0);

        executorService.shutdown();

        Util.close(fileWriter);

        entityReader.destroy();

        System.out.println("Extract process Completed.....");

        long finalTime = System.currentTimeMillis() - programStartTime;

        System.out.println("[Performance]: Total processing time : " + (finalTime / 1000) + "  Seconds");
    }

    /**
     * This method will process the List of object returned in the Scan response and
     * extract only required values and put the details in the Map format
     *
     * @param objects
     * @param attributes
     * @param lookUpAttributes
     * @return List<Map<String, String>>
     */
    public static List<Map<String, String>> getXrefResponse(List<ReltioObject> objects,
                                                            Map<String, InputAttribute> attributes, List<String> lookUpAttributes) {

        List<Map<String, String>> result = new ArrayList<>();

        objects.forEach(reltioObject -> {

            Map<String, String> responseMap = new HashMap<>();

            responseMap.put("ReltioURI", reltioObject.uri);

            attributes.entrySet().forEach(attributeEntry -> ExtractServiceUtil.createExtractOutputData(attributeEntry,
                    reltioObject.attributes.get(attributeEntry.getKey()), responseMap, null));

            result.add(responseMap);
        });

        return result;
    }

    /**
     * Converts a Map to an array of objects, adding only those entries whose key is
     * in the nameMapping array.
     *
     * @param values      the Map of values to convert
     * @param nameMapping the keys to extract from the Map (elements in the target
     *                    array will be added in this order)
     * @return the array of Objects
     * @throws NullPointerException if values or nameMapping is null
     */
    public static Object[] filterMapToObjectArray(final Map<String, ?> values, final String[] nameMapping) {

        if (values == null) {
            throw new NullPointerException("values should not be null");
        } else if (nameMapping == null) {
            throw new NullPointerException("nameMapping should not be null");
        }

        final Object[] targetArray = new Object[nameMapping.length];
        int i = 0;

        for (final String name : nameMapping) {
            targetArray[i++] = values.get(name);
        }
        return targetArray;
    }

    /**
     * Converts an Object array to a String array (null-safe), by calling toString()
     * on each element.
     *
     * @param objectArray the Object array
     * @return the String array, or null if objectArray is null
     */
    public static String[] objectArrayToStringArray(final Object[] objectArray) {

        if (objectArray == null) {
            return null;
        }

        final String[] stringArray = new String[objectArray.length];

        for (int i = 0; i < objectArray.length; i++) {
            stringArray[i] = objectArray[i] != null ? objectArray[i].toString() : null;
        }

        return stringArray;
    }
}
