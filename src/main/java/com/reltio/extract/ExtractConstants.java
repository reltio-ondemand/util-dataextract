/**
 *
 */
package com.reltio.extract;

import com.google.gson.Gson;

/**
 *
 *
 */
public final class ExtractConstants {

    public static final String SCAN_URL = "/entities/_dbscan?options=sendHidden";

    public static final String SCAN_INPUT = "{   \"type\" : \"configuration/entityTypes/:EntityType\",\"pageSize\" : MAX_PAGE_SIZE,   \"timeLimit\":TIME_LIMIT }";

    public static final String ENCODING = "UTF-8";

    public static final Integer THREAD_COUNT = 10;

}
