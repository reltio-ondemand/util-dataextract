/**
 *
 */
package com.reltio.extract.domain;

import com.reltio.cst.util.GenericUtilityService;
import com.reltio.cst.util.Util;
import com.reltio.extract.ExtractConstants;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 *
 */
public class ExtractProperties implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6543973273697083071L;

    private String apiUrl;
    private String authUrl;
    private String entityType;
    private String ovAttrFilePath;
    private String xrefAttrFilePath;
    private String outputFilePath;
    private String username;
    private String password;
    private String isHeaderRequired;
    private String fileFormat;
    private String fileDelimiter;
    private String serverHostName;
    private String tenantId;
    private String fromDate;
    private String toDate;
    private String relationType;
    private String attrFilePath;
    private Integer threadCount;
    private String timeLimit;
    private String recordsPerPost;

    private List<String> lookUpAttributes;

    public ExtractProperties(Properties properties) {
        // READ the Properties values
        serverHostName = properties.getProperty("ENVIRONMENT_URL");
        tenantId = properties.getProperty("TENANT_ID");
        if (!GenericUtilityService.checkNullOrEmpty(serverHostName)
                && !GenericUtilityService.checkNullOrEmpty(tenantId)) {
            apiUrl = "https://" + serverHostName + "/reltio/api/" + tenantId;
        }

        authUrl = properties.getProperty("AUTH_URL");
        entityType = properties.getProperty("ENTITY_TYPE");
        ovAttrFilePath = properties.getProperty("OV_ATTRIBUTE_FILE_LOCATION");
        xrefAttrFilePath = properties.getProperty("XREF_ATTRIBUTE_FILE_LOCATION");
        outputFilePath = properties.getProperty("OUTPUT_FILE");
        username = properties.getProperty("USERNAME");
        password = properties.getProperty("PASSWORD");
        isHeaderRequired = properties.getProperty("HEADER_REQUIRED");
        fileFormat = properties.getProperty("FILE_FORMAT");
        fileDelimiter = properties.getProperty("FILE_DELIMITER");

        if (!GenericUtilityService.checkNullOrEmpty(properties.getProperty("THREAD_COUNT"))) {
            threadCount = ExtractConstants.THREAD_COUNT;
        } else {
            threadCount = Integer.parseInt(properties.getProperty("THREAD_COUNT"));
        }
        fromDate = properties.getProperty("FROM_DATE");
        toDate = properties.getProperty("TO_DATE");
        relationType = properties.getProperty("RELATION_TYPE");
        attrFilePath = properties.getProperty("ATTRIBUTE_FILE_PATH");
        timeLimit = properties.getProperty("TIME_LIMIT", "4000");
        recordsPerPost = properties.getProperty("RECORDS_PER_POST", "100");

        if (properties.getProperty("LOOKUP_REQUIRED", "No").equalsIgnoreCase("Yes")) {
            String lookupAttribute = properties.getProperty("LOOK_UP_ATTRIBUTES");
            lookUpAttributes = Stream.of(lookupAttribute.split(",")).collect(Collectors.toList());
        }
        List<String> missings = Util.listMissingProperties(properties, Arrays.asList("AUTH_URL", "ENTITY_TYPE",
                "USERNAME", "PASSWORD", "OUTPUT_FILE", "FILE_FORMAT", "ENVIRONMENT_URL", "TENANT_ID"));

        if (Util.isNotEmpty(missings)) {
            System.out.println("Following properties are missing from configuration file!!\n" + missings);
            System.exit(0);
        }
    }

    public ExtractProperties(String serverHostName, String tenantId, String authUrl, String entityType, String username,
                             String password, String fileFormat, String fileDelimiter, String isHeaderRequired) {

        // READ the Properties values
        this.serverHostName = serverHostName;
        this.tenantId = tenantId;

        if (!GenericUtilityService.checkNullOrEmpty(serverHostName)
                && !GenericUtilityService.checkNullOrEmpty(tenantId)) {
            this.apiUrl = "https://" + serverHostName + "/reltio/api/" + tenantId;
        }

        this.authUrl = authUrl;
        this.entityType = entityType;

        this.username = username;
        this.password = password;
        this.isHeaderRequired = isHeaderRequired;
        this.fileFormat = fileFormat;
        this.fileDelimiter = fileDelimiter;

        threadCount = ExtractConstants.THREAD_COUNT;

        this.outputFilePath = "/tmp/" + serverHostName + "_" + tenantId + "_" + entityType + "_" + username + "_"
                + new Date().getTime();
        if (fileFormat.equalsIgnoreCase("CSV")) {
            this.outputFilePath += ".csv";
        } else {
            this.outputFilePath += ".txt";
        }

    }

    public String getApiUrl() {
        return apiUrl;
    }


    public String getOvAttrFilePath() {
        return ovAttrFilePath;
    }

    public String getXrefAttrFilePath() {
        return xrefAttrFilePath;
    }

    public String getOutputFilePath() {
        return outputFilePath;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getIsHeaderRequired() {
        return isHeaderRequired;
    }

    public String getFileFormat() {
        return fileFormat;
    }

    public String getFileDelimiter() {
        return fileDelimiter;
    }

    public String getFromDate() {
        return fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public String getRelationType() {
        return relationType;
    }

    public String getAttrFilePath() {
        return attrFilePath;
    }

    public Integer getThreadCount() {
        return threadCount;
    }

    public List<String> getLookUpAttributes() {
        return lookUpAttributes;
    }
}
