package com.reltio.extract.domain;

import java.util.List;

import com.reltio.cst.domain.Attribute;

public class ScanResponseURI {

	private List<ReltioObject> objects;

	private Attribute cursor;

	public List<ReltioObject> getObjects() {
		return objects;
	}

	public void setObjects(List<ReltioObject> objects) {
		this.objects = objects;
	}

	public Attribute getCursor() {
		return cursor;
	}

	public void setCursor(Attribute cursor) {
		this.cursor = cursor;
	}
}
