package com.reltio.extract.service.readers;

/**
 * @author Shivaputra Patil
 * This class will iterates exported file with pagination
 */

import java.io.BufferedReader;
import java.util.List;
import java.util.Properties;

import com.reltio.cst.service.ReltioAPIService;
import com.reltio.cst.util.Util;
import com.reltio.extract.domain.ReltioObject;
import com.reltio.extract.util.ExportFileReaderHelper;
import com.reltio.rocs.main.Exporter;
@Deprecated
public class CassandraExportFileEntityReader extends ESExportFileEntityReader {

    protected String filePath;

    private boolean isInitialized = false;

    private List<BufferedReader> readers;

    private BufferedReader currentReader;

    private int recordsPerRequest = 100;

    public CassandraExportFileEntityReader(Properties config, ReltioAPIService service) {
        super(config, service);
    }

    @Override
    public List<ReltioObject> getReltioObjects() throws Exception {

        if (!isInitialized) {
            isInitialized = true;
            recordsPerRequest = Integer.parseInt(getConfig().getProperty("RECORDS_PER_POST", "200"));
            filePath = exportEntities();
            readers = readZipFile(filePath);
            currentReader = readers.get(0);
        }

        List<ReltioObject> result = getJsonToReltioObject();

        return result;
    }

    protected List<ReltioObject> getJsonToReltioObject() throws Exception {

        List<ReltioObject> results = ExportFileReaderHelper.readLines(recordsPerRequest, currentReader);

        if (results.size() == 0) {

            readers.remove(currentReader);

            if (readers.size() > 0) {
                currentReader = readers.get(0);
                return getJsonToReltioObject();
            }
        }

        return results;
    }

    /**
     * This will initiate export api call and download file
     */
    protected String exportEntities() throws Exception {
        Properties exportConfig = prepareExportConfig();
        Exporter exporter = new Exporter();
        filePath = exporter.exportData(exportConfig).get(0);
        // TODO: 2/7/19 Backward compatibility used above; but needs to be changed for v2 of export
        return filePath;
    }

    private Properties prepareExportConfig() {

        Properties exportConfig = new Properties(getConfig());

        exportConfig.put("FILE_FORMAT", "json");
        exportConfig.put("EXPORT_TYPE", "cassandraexport");
        exportConfig.put("FILTER", getFilter());

        return exportConfig;
    }

    /**
     * This will prepare filter with entity type passed in configuration already
     */
    private String getFilter() {

        String filter = getConfig().getProperty("FILTER");

        if (filter != null && filter.contains("equals(type,")) {
            System.out.println("entity is specified in FILTER and also in ENTITY_TYPE!!");
            System.exit(0);

        } else if (filter == null) {
            filter = "equals(type,'configuration/entityTypes/" + getConfig().getProperty("ENTITY_TYPE") + "')";
        } else {
            filter += " and equals(type,'configuration/entityTypes/" + getConfig().getProperty("ENTITY_TYPE") + "')";
        }

        return filter;
    }

    public void destroy() {
        BufferedReader[] reader = (BufferedReader[]) readers.toArray();
        Util.close(reader);
    }
}
