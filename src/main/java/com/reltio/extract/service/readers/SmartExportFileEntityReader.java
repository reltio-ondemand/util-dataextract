package com.reltio.extract.service.readers;

import java.io.BufferedReader;
import java.util.List;
import java.util.Properties;

/**
 * @author Vignesh Chandran
 * This Class is for the Support for Smart Export in Reltio
 */

import com.reltio.cst.service.ReltioAPIService;
import com.reltio.cst.util.Util;
import com.reltio.extract.domain.ReltioObject;
import com.reltio.extract.util.ExportFileReaderHelper;
import com.reltio.rocs.main.Exporter;

public class SmartExportFileEntityReader extends EntityReader {

	protected List<String> filePath;
	private boolean isInitialized = false;
	private List<BufferedReader> readers;
	private BufferedReader currentReader;
	private int recordsPerRequest = 100;

	public SmartExportFileEntityReader(Properties config, ReltioAPIService service) {
		super(config, service);
	}

	@Override
	public List<ReltioObject> getReltioObjects() throws Exception {

		if (!isInitialized) {

			isInitialized = true;
			recordsPerRequest = Integer.parseInt(getConfig().getProperty("RECORDS_PER_POST", "200"));
			filePath = exportEntities();
			
			
			readers = FileReaderBuilder.compressedFileReaders(filePath);;
			currentReader = readers.get(0);
		}

		return getJsonToReltioObject();
	}

	protected List<ReltioObject> getJsonToReltioObject() throws Exception {

		List<ReltioObject> results = ExportFileReaderHelper.readLines(recordsPerRequest, currentReader);
		if (results.size() == 0) {
			readers.remove(currentReader);
			if (readers.size() > 0) {
				currentReader = readers.get(0);
				return getJsonToReltioObject();
			}
		}

		return results;
	}

	/**
	 * This will initiate export api call and download file
	 */
	protected List<String> exportEntities() throws Exception {

		Properties exportConfig = prepareExportConfig();
		Exporter exporter = new Exporter();
		filePath = exporter.exportData(exportConfig);
		return filePath;
	}

	private Properties prepareExportConfig() {

		Properties exportConfig = new Properties(getConfig());
		exportConfig.put("FILTER", getFilter());
		exportConfig.put("FILE_FORMAT", "json");
		exportConfig.put("EXPORT_TYPE", "esexport");
		exportConfig.put("VERSION", "v2");
		return exportConfig;
	}


	/**
	 * This method will prepare filter with filter, entity type passed in
	 * configuration
	 */
	private String getFilter() {

		String filter = getConfig().getProperty("FILTER");

		if (filter != null && filter.contains("equals(type,")) {
			System.out.println("entity is specified in FILTER and also in ENTITY_TYPE!!");
			System.exit(0);

		} else if (filter == null) {
			filter = "equals(type,'configuration/entityTypes/" + getConfig().getProperty("ENTITY_TYPE") + "')";
		} else {
			filter += " and equals(type,'configuration/entityTypes/" + getConfig().getProperty("ENTITY_TYPE") + "')";
		}

		return filter;
	}

	public void destroy() {
		for (BufferedReader reader : readers) {
			Util.close(reader);
		}
	}
}