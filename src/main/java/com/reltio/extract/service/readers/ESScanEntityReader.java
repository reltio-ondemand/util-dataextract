package com.reltio.extract.service.readers;

/**
 * @author Shivaputra Patil
 *This class will give entities by ES scan, accepts filter and max values to return no of Object on each request  
 */
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang3.math.NumberUtils;

import com.google.gson.Gson;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.extract.domain.ReltioObject;
import com.reltio.extract.domain.ScanResponseURI;

public class ESScanEntityReader extends EntityReader {

	private boolean isInitialized;

	private String intialJSON = "";

	private ScanResponseURI scanResponse;

	private Gson GSON = new Gson();

	private String url;

	public ESScanEntityReader(Properties config, ReltioAPIService service) {
		super(config, service);
	}

	@Override
	public List<ReltioObject> getReltioObjects() throws Exception {

		List<ReltioObject> result = Collections.emptyList();

		if (!isInitialized) {

			isInitialized = true;
			result = initialize();

		} else {

			String entitiesJson = getService().post(url, intialJSON);
			scanResponse = GSON.fromJson(entitiesJson, ScanResponseURI.class);
			result = scanResponse.getObjects();

			scanResponse.setObjects(null);

			intialJSON = GSON.toJson(scanResponse.getCursor());

			intialJSON = "{\"cursor\":" + intialJSON + "}";
		}

		return result;
	}

	/** This will initialize es scan indexing */
	private List<ReltioObject> initialize() throws Exception {

		int bulk = NumberUtils.toInt(getConfig().getProperty("RECORDS_PER_POST"), 75);

		url = getApiUrl() + "/entities/_scan?filter=" + getFilter() + "&select="
				+ getConfig().getProperty("SELECT", "uri") + "&max=" + bulk;

		String entitiesJson = getService().post(url, intialJSON);
		scanResponse = GSON.fromJson(entitiesJson, ScanResponseURI.class);

		List<ReltioObject> result = scanResponse.getObjects();

		scanResponse.setObjects(null);

		intialJSON = GSON.toJson(scanResponse.getCursor());

		intialJSON = "{\"cursor\":" + intialJSON + "}";

		return result;
	}

	/**
	 * This will prepare filter with entity type passed in configuration already
	 */
	private String getFilter() {

		String filter = getConfig().getProperty("FILTER");

		if (filter != null && filter.contains("equals(type,")) {
			System.out.println("entity is specified in FILTER and also in ENTITY_TYPE!!");
			System.exit(0);

		} else if (filter == null) {
			filter = "equals(type,'configuration/entityTypes/" + getConfig().getProperty("ENTITY_TYPE") + "')";
		} else {
			filter += " and equals(type,'configuration/entityTypes/" + getConfig().getProperty("ENTITY_TYPE") + "')";
		}

		return filter;
	}

}
