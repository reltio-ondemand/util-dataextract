package com.reltio.extract.service.readers;

/**
 * @author Shivaputrappa Patil
 *
 *This class is abstract class for reading data from reltio
 */
import java.util.List;
import java.util.Properties;

import com.reltio.cst.service.ReltioAPIService;
import com.reltio.extract.domain.ReltioObject;

public abstract class EntityReader {

	private Properties config;

	private ReltioAPIService service;

	public EntityReader(Properties config, ReltioAPIService service) {
		this.config = config;
		this.service = service;
	}

	public String getApiUrl() {
		return "https://" + config.getProperty("ENVIRONMENT_URL") + "/reltio/api/" + config.getProperty("TENANT_ID")
				+ "/";
	}

	public Properties getConfig() {
		return config;
	}

	public void setConfig(Properties config) {
		this.config = config;
	}

	public ReltioAPIService getService() {
		return service;
	}

	public void setService(ReltioAPIService service) {
		this.service = service;
	}

	public abstract List<ReltioObject> getReltioObjects() throws Exception;

	public void destroy(){

	}
}
