package com.reltio.extract.service.readers;

/**
 * @author Shivaputra Patil
 *
 */
import static com.reltio.extract.ExtractConstants.SCAN_INPUT;
import static com.reltio.extract.ExtractConstants.SCAN_URL;

import java.util.Collections;
import java.util.List;
import java.util.Properties;

import com.google.gson.Gson;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.extract.domain.ReltioObject;
import com.reltio.extract.domain.ScanResponse;
import com.reltio.extract.util.ExtractServiceUtil;

public class DBScanEntityReader extends EntityReader {

	private boolean isInitialized;

	private String intitialJSON;

	private ScanResponse scanResponse;

	private Gson GSON = new Gson();

	public DBScanEntityReader(Properties config, ReltioAPIService service) {
		super(config, service);
	}

	@Override
	public List<ReltioObject> getReltioObjects() throws Exception {

		List<ReltioObject> result = Collections.emptyList();

		if (!isInitialized) {

			isInitialized = true;
			result = initialize();

		} else {

			scanResponse.setObjects(null);
			intitialJSON = GSON.toJson(scanResponse.getCursor());
			String response = getService().post(getApiUrl() + SCAN_URL, intitialJSON);

			scanResponse = GSON.fromJson(response, ScanResponse.class);

			result = scanResponse.getObjects();
		}

		return result;
	}

	private List<ReltioObject> initialize() throws Exception {

		intitialJSON = SCAN_INPUT.replaceAll(":EntityType", getConfig().getProperty("ENTITY_TYPE"));

		intitialJSON = ExtractServiceUtil.overrideScanProperties(getConfig(), intitialJSON);

		String response = getService().post(getApiUrl() + SCAN_URL, intitialJSON);
		scanResponse = GSON.fromJson(response, ScanResponse.class);

		return scanResponse.getObjects();
	}

}
