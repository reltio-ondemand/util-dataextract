package com.reltio.extract.service.readers;

/**
 * @author Shivaputra Patil
 * This class will iterates exported file with pagination
 */

import com.reltio.cst.service.ReltioAPIService;
import com.reltio.cst.util.Util;
import com.reltio.extract.domain.ReltioObject;
import com.reltio.extract.util.ExportFileReaderHelper;
import com.reltio.rocs.main.Exporter;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class ExportedFileEntityReader extends ESExportFileEntityReader {

    protected String filePath;

    private boolean isInitialized = false;

    private List<BufferedReader> readers;

    private BufferedReader currentReader;

    private int recordsPerRequest = 100;

    public ExportedFileEntityReader(Properties config, ReltioAPIService service) {
        super(config, service);
    }

    @Override
    public List<ReltioObject> getReltioObjects() throws Exception {

        if (!isInitialized) {

            isInitialized = true;
            recordsPerRequest = Integer.parseInt(getConfig().getProperty("RECORDS_PER_POST", "200"));
            filePath = getConfig().getProperty("EXPORTEDFILE_PATH");
            readers = readZipFile(filePath);
            currentReader = readers.get(0);
        }

        return getJsonToReltioObject();
    }


}