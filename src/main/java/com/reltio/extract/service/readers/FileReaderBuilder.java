/**
 *
 */
package com.reltio.extract.service.readers;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * @author spatil
 *
 */
public abstract class FileReaderBuilder {

	public static List<BufferedReader> compressedFileReaders(List<String> files) throws Exception {

		List<BufferedReader> readers = new ArrayList<>();

		if (files.get(0).endsWith("gz")) {

			for (String singleFile : files) {
				InputStream fileStream = new FileInputStream(singleFile);
				InputStream gzipStream = new GZIPInputStream(fileStream);
				Reader decoder = new InputStreamReader(gzipStream, StandardCharsets.UTF_8);
				BufferedReader reader = new BufferedReader(decoder);
				readers.add(reader);
			}

		} else if (files.get(0).endsWith("zip")) {

			for (String zipFile : files) {

				ZipFile zf = new ZipFile(zipFile);
				ZipEntry ze = (ZipEntry) zf.entries().nextElement();
				BufferedReader reader = new BufferedReader(new InputStreamReader(zf.getInputStream(ze), StandardCharsets.UTF_8));
				readers.add(reader);
			}
		}

		return readers;
	}
}
