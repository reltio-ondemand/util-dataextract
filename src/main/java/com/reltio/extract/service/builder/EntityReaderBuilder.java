/**
 *
 */
package com.reltio.extract.service.builder;

import java.util.Properties;

import com.reltio.cst.service.ReltioAPIService;
import com.reltio.extract.service.readers.DBScanEntityReader;
import com.reltio.extract.service.readers.ESScanEntityReader;
import com.reltio.extract.service.readers.EntityReader;
import com.reltio.extract.service.readers.ExportedFileEntityReader;
import com.reltio.extract.service.readers.SmartExportFileEntityReader;

/**
 * @author spatil
 *
 */
public class EntityReaderBuilder {

    public static EntityReader build(Properties config, ReltioAPIService service) {

        EntityReader result = null;

        String type = config.getProperty("EXTRACT_TYPE", "");

        switch (type) {

            case "esexport": {
            	throw new RuntimeException("esexport is not supported Please use v2export");
               // result = new ESExportFileEntityReader(config, service);
                //break;
            }

            case "exportedfile": {
                result = new ExportedFileEntityReader(config, service);
                break;
            }

            case "dbscan": {
                result = new DBScanEntityReader(config, service);
                break;
            }

            case "esscan": {
                result = new ESScanEntityReader(config, service);
                break;
            }

            case "cassandraexport": {
            	
            	throw new RuntimeException("cassandraexport is not supported Please use v2export");
                //result = new CassandraExportFileEntityReader(config, service);
               // break;
            }
            case "v2export": {
                result = new SmartExportFileEntityReader(config, service);
                break;
            }

            default: {
                result = new DBScanEntityReader(config, service);
                break;
            }
        }

        return result;
    }
}
