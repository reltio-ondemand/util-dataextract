
package com.reltio.extract.util;

import com.google.gson.internal.LinkedTreeMap;
import com.reltio.cst.util.Util;
import com.reltio.extract.domain.Crosswalk;
import com.reltio.extract.domain.InputAttribute;
import com.reltio.extract.domain.XrefInputAttribute;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.*;


public class ExtractServiceUtil {

    private static final String LOOKUP_CODE_SUFFIX = " (LookUpCode)";

    public static void createAttributeMapFromProperties(BufferedReader reader, Map<String, InputAttribute> attributes)
            throws IOException {

        String line;

        while ((line = reader.readLine()) != null) {

            int noOfValues = 1;

            if (line.contains("=")) {

                line = line.trim();
                String[] attrs = line.split("=", -1);
                if (attrs[1] != null && !attrs[1].isEmpty()) {
                    noOfValues = Integer.parseInt(attrs[1]);
                }
                line = attrs[0].trim();
            }
            if (line.contains(".")) {
                String[] nestAttrs1 = line.split("\\.", -1);
                InputAttribute attribute = attributes.get(nestAttrs1[0]);
                if (attribute == null) {
                    attribute = new InputAttribute();
                }
                createNestedExtractAttribute(nestAttrs1, 1, attribute, noOfValues);
                attributes.put(nestAttrs1[0], attribute);

            } else {
                InputAttribute inputAttribute = new InputAttribute();
                inputAttribute.count = noOfValues;
                attributes.put(line, inputAttribute);
            }
        }
    }

    private static void createNestedExtractAttribute(String[] attrs, Integer index, InputAttribute attribute,
                                                     Integer noOfValues) {

        String attrName = attrs[index];

        if (attribute.attributesMap == null) {
            attribute.attributesMap = new LinkedHashMap<>();
        }
        InputAttribute nestAttr = attribute.attributesMap.get(attrName);

        if (nestAttr == null) {
            nestAttr = new InputAttribute();
        }

        if (attrs.length > (index + 1)) {
            createNestedExtractAttribute(attrs, index + 1, nestAttr, noOfValues);
        } else {
            nestAttr.count = noOfValues;
        }
        attribute.attributesMap.put(attrName, nestAttr);
    }

    public static void createExtractNestedResponseHeader(Map.Entry<String, InputAttribute> nestedAttrs,
                                                         List<String> fileResponseHeader, String headerPrefix, List<String> lookUpAttributes) {
        if (headerPrefix != null) {
            headerPrefix += ".";
        } else {
            headerPrefix = "";
        }
        headerPrefix += nestedAttrs.getKey();

        InputAttribute inputAttribute = nestedAttrs.getValue();

        if (inputAttribute.count > 1) {

            for (int i = 1; i <= inputAttribute.count; i++) {

                if (inputAttribute.attributesMap == null) {

                    fileResponseHeader.add(headerPrefix + "_" + i);

                    if (lookUpAttributes != null && lookUpAttributes.contains(nestedAttrs.getKey())) {
                        fileResponseHeader.add(headerPrefix + "_" + i + LOOKUP_CODE_SUFFIX);
                    }

                } else {

                    for (Map.Entry<String, InputAttribute> innerNestedAttrs : inputAttribute.attributesMap.entrySet()) {
                        createExtractNestedResponseHeader(innerNestedAttrs, fileResponseHeader, headerPrefix + "_" + i, lookUpAttributes);
                    }
                }
            }

        } else {

            if (inputAttribute.attributesMap == null) {

                fileResponseHeader.add(headerPrefix);

                if (lookUpAttributes != null && lookUpAttributes.contains(nestedAttrs.getKey())) {
                    fileResponseHeader.add(headerPrefix + LOOKUP_CODE_SUFFIX);
                }

            } else {
                for (Map.Entry<String, InputAttribute> innerNestedAttrs : inputAttribute.attributesMap.entrySet()) {
                    createExtractNestedResponseHeader(innerNestedAttrs, fileResponseHeader, headerPrefix, lookUpAttributes);
                }
            }
        }

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public static void createExtractOutputData(Map.Entry<String, InputAttribute> attribute, List<Object> data,
                                               Map<String, String> responseMap, String headerPrefix) {

        if (headerPrefix != null) {
            headerPrefix += ".";
        } else {
            headerPrefix = "";
        }
        headerPrefix += attribute.getKey();
        InputAttribute inputAttribute = attribute.getValue();

        int attributeCount = 1;

        if (inputAttribute.attributesMap == null) {

            if (data != null && !data.isEmpty()) {

                for (Object obj : data) {

                    if (attributeCount > inputAttribute.count) {
                        break;
                    }

                    LinkedTreeMap object2 = (LinkedTreeMap) obj;

                    String value = (String) object2.get("value");

                    if ((Boolean) object2.get("ov")) {

                        String lookupcode = (String) object2.get("lookupCode");

                        if (inputAttribute.count > 1) {
                            responseMap.put(headerPrefix + "_" + attributeCount, value);
                            responseMap.put(headerPrefix + "_" + attributeCount + LOOKUP_CODE_SUFFIX, lookupcode);

                            attributeCount++;
                        } else {
                            responseMap.put(headerPrefix, value);
                            responseMap.put(headerPrefix + LOOKUP_CODE_SUFFIX, lookupcode);
                            break;
                        }
                    }
                }
            }
        } else {

            if (Util.isNotEmpty(data)) {

                for (Object obj : data) {

                    if (attributeCount > inputAttribute.count) {
                        break;
                    }

                    LinkedTreeMap object2 = (LinkedTreeMap) obj;
                    Boolean ov = (Boolean) object2.get("ov");
                    Map<String, List<Object>> innerAttrs = (Map<String, List<Object>>) object2.get("value");

                    if (ov) {

                        if (inputAttribute.count > 1) {

                            for (Map.Entry<String, InputAttribute> inputAttr : inputAttribute.attributesMap
                                    .entrySet()) {
                                List<Object> objects3 = innerAttrs.get(inputAttr.getKey());
                                createExtractOutputData(inputAttr, objects3, responseMap, headerPrefix + "_" + attributeCount);
                            }
                        } else {
                            for (Map.Entry<String, InputAttribute> inputAttr : inputAttribute.attributesMap
                                    .entrySet()) {
                                List<Object> objects3 = innerAttrs.get(inputAttr.getKey());
                                createExtractOutputData(inputAttr, objects3, responseMap, headerPrefix);
                            }
                            break;
                        }
                        attributeCount++;
                    }
                }
            }
        }
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public static void createUriValuesData(Map.Entry<String, InputAttribute> attribute, List<Object> data,
                                           Map<String, List<XrefInputAttribute>> responseMap) {

        InputAttribute inputAttribute = attribute.getValue();

        // Simple Attribute
        if (inputAttribute.attributesMap == null) {

            if (Util.isNotEmpty(data)) {

                XrefInputAttribute newMapAttr = new XrefInputAttribute();
                newMapAttr.uriValuesMap = new LinkedHashMap<>();
                newMapAttr.count = inputAttribute.count;
                List<XrefInputAttribute> xrefInputAttributes = responseMap.get(attribute.getKey());

                if (xrefInputAttributes == null) {
                    xrefInputAttributes = new ArrayList<>();
                }

                data.forEach(obj -> {

                    LinkedTreeMap object2 = (LinkedTreeMap) obj;
                    String value = (String) object2.get("value");
                    String uri = (String) object2.get("uri");
                    String lookUpCode = (String) object2.get("lookupCode");

                    if (lookUpCode != null) {
                        newMapAttr.uriValuesMap.put(uri + LOOKUP_CODE_SUFFIX, lookUpCode);
                    }
                    newMapAttr.uriValuesMap.put(uri, value);
                });

                xrefInputAttributes.add(newMapAttr);
                responseMap.put(attribute.getKey(), xrefInputAttributes);
            }

        } else {
            if (data != null && !data.isEmpty()) {
                List<XrefInputAttribute> xrefInputAttributes = responseMap.get(attribute.getKey());

                if (xrefInputAttributes == null) {
                    xrefInputAttributes = new ArrayList<>();
                }

                for (Object obj : data) {

                    XrefInputAttribute newMapAttr = new XrefInputAttribute();
                    newMapAttr.attributesMap = new LinkedHashMap<>();
                    newMapAttr.count = inputAttribute.count;
                    LinkedTreeMap object2 = (LinkedTreeMap) obj;
                    Map<String, List<Object>> innerAttrs = (Map<String, List<Object>>) object2.get("value");
                    Object startObjCross = object2.get("startObjectCrosswalks");

                    if (startObjCross != null) {
                        List<Object> objects = (List<Object>) startObjCross;
                        newMapAttr.isReference = true;
                        newMapAttr.refCrosswalks = new ArrayList<>();


                        objects.forEach(crossObj -> {
                            Map<String, Object> crosswalkValues = (Map<String, Object>) crossObj;

                            Crosswalk crosswalk = new Crosswalk();

                            crosswalk.type = (String) crosswalkValues.get("type");
                            crosswalk.value = (String) crosswalkValues.get("value");

                            newMapAttr.refCrosswalks.add(crosswalk);
                        });

                    }

                    for (Map.Entry<String, InputAttribute> inputAttr : inputAttribute.attributesMap.entrySet()) {
                        List<Object> objects3 = innerAttrs.get(inputAttr.getKey());
                        createUriValuesData(inputAttr, objects3, newMapAttr.attributesMap);
                    }

                    xrefInputAttributes.add(newMapAttr);
                }

                responseMap.put(attribute.getKey(), xrefInputAttributes);
            }
        }

    }

    public static void createXrefExtractOutputData(Map.Entry<String, List<XrefInputAttribute>> attribute,
                                                   final Crosswalk crosswalk, Map<String, String> responseMap, String headerPrefix) {
        if (headerPrefix != null) {
            headerPrefix += ".";
        } else {
            headerPrefix = "";
        }
        headerPrefix += attribute.getKey();
        List<XrefInputAttribute> inputAttributes = attribute.getValue();
        List<String> attributesUrisTobePassed = null;
        if (crosswalk != null) {
            attributesUrisTobePassed = crosswalk.attributes;
        }

        Crosswalk crosswalkToSend = crosswalk;

        if (inputAttributes != null && !inputAttributes.isEmpty()) {
            // Simple Attribute
            if (inputAttributes.size() == 1 && inputAttributes.get(0).attributesMap == null) {
                XrefInputAttribute inputAttribute = inputAttributes.get(0);

                if (inputAttribute.uriValuesMap != null) {
                    int temp = 1;
                    for (Map.Entry<String, String> uriValues : inputAttribute.uriValuesMap.entrySet()) {

                        if (temp > inputAttribute.count) {
                            break;
                        }

                        if (attributesUrisTobePassed == null || attributesUrisTobePassed.contains(uriValues.getKey())) {

                            if (inputAttribute.count > 1) {

                                int number = temp;

                                responseMap.put(headerPrefix + "_" + number, uriValues.getValue());
                                responseMap.put(headerPrefix + LOOKUP_CODE_SUFFIX + "_" + number, inputAttribute.uriValuesMap.get(uriValues.getKey() + LOOKUP_CODE_SUFFIX));

                                temp++;

                            } else {

                                responseMap.put(headerPrefix, uriValues.getValue());
                                responseMap.put(headerPrefix + LOOKUP_CODE_SUFFIX, inputAttribute.uriValuesMap.get(uriValues.getKey() + " (LookUpCode)"));

                                break;
                            }
                        }
                    }
                }
            } else {
                int temp = 1;
                int initialSize;
                for (XrefInputAttribute inputAttribute : inputAttributes) {
                    if (temp > inputAttribute.count) {
                        break;
                    }

                    // Checks whether the attribute is reference
                    if (inputAttribute.isReference) {
                        boolean isRefPartOfSrcCrosswalk = false;
                        for (Crosswalk refCrosswalk : inputAttribute.refCrosswalks) {
                            // Checks whether reference attribute is part of the current crosswalk
                            if (refCrosswalk.type.equalsIgnoreCase(crosswalk.type)
                                    && refCrosswalk.value.equalsIgnoreCase(crosswalk.value)) {
                                isRefPartOfSrcCrosswalk = true;
                                break;
                            }
                        }
                        if (isRefPartOfSrcCrosswalk) {
                            crosswalkToSend = null;
                        } else {
                            continue;
                        }
                    }
                    initialSize = responseMap.size();

                    if (inputAttribute.count > 1) {

                        for (Map.Entry<String, List<XrefInputAttribute>> inputAttr : inputAttribute.attributesMap
                                .entrySet()) {
                            createXrefExtractOutputData(inputAttr, crosswalkToSend, responseMap,
                                    headerPrefix + "_" + temp);
                        }
                    } else {

                        for (Map.Entry<String, List<XrefInputAttribute>> inputAttr : inputAttribute.attributesMap
                                .entrySet()) {
                            createXrefExtractOutputData(inputAttr, crosswalkToSend, responseMap, headerPrefix);
                        }
                        if (initialSize < responseMap.size()) {
                            break;
                        }
                    }
                    if (initialSize < responseMap.size()) {
                        temp++;
                    }
                }
            }
        }
    }

    public static String overrideScanProperties(final Properties extractProperties, final String payload) {

        String result = payload.replaceAll("TIME_LIMIT", extractProperties.getProperty("TIME_LIMIT", "4000"));

        result = result.replaceAll("MAX_PAGE_SIZE",
                extractProperties.getProperty("RECORDS_PER_POST", "100"));

        return result;
    }
}
