/**
 * 
 */
package com.reltio.extract.util;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.reltio.extract.domain.ReltioObject;

/**
 * @author Shivaputra Patil
 *
 */
public abstract class ExportFileReaderHelper {

	public static List<ReltioObject> readLines(int size, BufferedReader reader) throws Exception {

		List<ReltioObject> result = new ArrayList<>(size);

		String line = "";

		while (size > result.size() && (line = reader.readLine()) != null) {

			line = line.trim();

			if (line.equals("[") || line.equals("]")) {
				continue;
			}

			try {

				String json = line;

				if (line.length() != 0 && !line.startsWith("#")) {
					if (line.endsWith(",")) {
						json = line.substring(0, line.length() - 1);
					}

					result.add(new Gson().fromJson(json, ReltioObject.class));
				}

			} catch (Exception e) {
				System.out.println(e);
			}
		}

		return result;
	}
}
