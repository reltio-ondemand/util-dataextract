package com.reltio.relations;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.gson.Gson;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.cst.util.Util;
import com.reltio.extract.domain.InputAttribute;
import com.reltio.extract.domain.ScanResponse;
import com.reltio.extract.util.ExtractServiceUtil;
import com.reltio.file.ReltioFileWriter;
import com.reltio.file.ReltioFlatFileWriter;
import com.reltio.util.PropertyUtil;

public class RelationExtractByDBScanSingleThread {

    public static final String SCAN_INPUT = "{\"type\":\"configuration/relationTypes/:RelationType\",\"pageSize\":MAX_PAGE_SIZE ,  \"timeLimit\":TIME_LIMIT}";
    private static final String[] DefaultAttributes = {"RelationURI", "RelationType", "StartObjectUri",
            "EndObjectUri"};
    private static final Gson GSON = new Gson();
    private static String previousCursorValue;

    public static void main(String[] args) throws Exception {

        long programStartTime = System.currentTimeMillis();
        System.out.println("Process Started");

        if (args.length == 0) {
            System.out.println("Please pass file path as argument!!!");
            System.exit(0);
        }
        // Encrypting Password
        Properties properties = Util.getProperties(args[0], "PASSWORD", "CLIENT_CREDENTIALS");

        List<String> requiredKeys = Arrays.asList("AUTH_URL", "OUTPUT_FILE", "ENVIRONMENT_URL", "ATTRIBUTE_FILE",
                "TENANT_ID", "RELATION_TYPE");

        // Missing Keys

        List<String> missingKeys = Util.listMissingProperties(properties, requiredKeys,
                PropertyUtil.getMutualExclusiveProps());

        if (!Util.isEmpty(missingKeys)) {
            System.out.println("Properties are missing \n" + String.join("\n", missingKeys));
            System.exit(0);
        }

        String environmentUrl = properties.getProperty("ENVIRONMENT_URL").trim();
        String tenantId = properties.getProperty("TENANT_ID").trim();
        String outputFilePath = properties.getProperty("OUTPUT_FILE").trim();
        String attributeFilePath = properties.getProperty("ATTRIBUTE_FILE").trim();
        String relationType = properties.getProperty("RELATION_TYPE").trim();
        String fileDelimiter = properties.getProperty("FILE_DELIMITER", ",").trim();
        String timeLimit = properties.getProperty("TIME_LIMIT", "1000").trim();
        String recordsPerPost = properties.getProperty("RECORDS_PER_POST", "100").trim();
        List<String> lookUpAttributes = Collections.emptyList();

        if (properties.getProperty("LOOKUP_REQUIRED", "No").equalsIgnoreCase("Yes")) {
            String lookupAttribute = properties.getProperty("LOOK_UP_ATTRIBUTES");
            lookUpAttributes = Stream.of(lookupAttribute.split(",")).collect(Collectors.toList());
        }
        // Proxy setup
        if (!Util.isEmpty(properties.getProperty("HTTP_PROXY_HOST"))
                && !Util.isEmpty(properties.getProperty("HTTP_PROXY_PORT"))) {
            Util.setHttpProxy(properties);
        }

        final ReltioAPIService reltioAPIService = Util.getReltioService(properties);
        Integer count = 0;

        final Map<String, com.reltio.extract.domain.InputAttribute> attributes = new LinkedHashMap<>();
        final ReltioFileWriter writer = new ReltioFlatFileWriter(outputFilePath, "UTF-8", fileDelimiter);

        // Read OV Values Attribute
        if (attributeFilePath != null && !attributeFilePath.equalsIgnoreCase("null")) {
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(new FileInputStream(attributeFilePath), StandardCharsets.UTF_8));
            ExtractServiceUtil.createAttributeMapFromProperties(reader, attributes);
            Util.close(reader);
        }

        final List<String> fileResponseHeader = new ArrayList<>();

        Collections.addAll(fileResponseHeader, DefaultAttributes);

        // Create Response file header
        for (Map.Entry<String, InputAttribute> attr : attributes.entrySet()) {
            ExtractServiceUtil.createExtractNestedResponseHeader(attr, fileResponseHeader, null, null);
        }

        final String[] responseHeader = new String[fileResponseHeader.size()];
        fileResponseHeader.toArray(responseHeader);

        writer.writeToFile(responseHeader);

        final String scanUrl =  "https://" + environmentUrl + "/reltio/api/" + tenantId + "/relations/_dbscan";
        System.out.println(scanUrl);

        String initialjson = SCAN_INPUT.replaceAll(":RelationType", relationType).replaceAll("TIME_LIMIT", timeLimit)
                .replaceAll("MAX_PAGE_SIZE", recordsPerPost);

        boolean eof = false;

        while (!eof) {

            // Doing the DBScan API Call here
            String scanResponse = reltioAPIService.post(scanUrl, initialjson);

            // Convert the string the java object
            ScanResponse scanResponseObj = GSON.fromJson(scanResponse, ScanResponse.class);
            String currentCursorValue = scanResponseObj.getCursor().getCursor();

            if (scanResponseObj.getObjects() == null) {
                System.out.println("RelationExtractByDBScanSingleThread: Scan Response Object is null\n" + scanResponse);
            } else {
                System.out.println("RelationExtractByDBScanSingleThread: Scan Response Object is not null and Object Size = " + scanResponseObj.getObjects().size());
            }
            if (currentCursorValue.equals(previousCursorValue) && scanResponseObj.getObjects() == null) {
                System.out.println("Cursors are same");
                eof = true;
                break;
            } else {
                previousCursorValue = currentCursorValue;
                if (scanResponseObj.getObjects() != null) {
                    count += scanResponseObj.getObjects().size();
                    System.out.println("Scanned records count = " + count);

                    // System.out.println("Object is not null");
                    final List<com.reltio.extract.domain.ReltioObject> objectsToProcess = scanResponseObj.getObjects();

                    for (final com.reltio.extract.domain.ReltioObject reltioObject : objectsToProcess) {
                        try {

                            String relationName = reltioObject.type.replace("configuration/relationTypes/", "");

                            List<Map<String, String>> finalResponseMap = new ArrayList<>();

                            Map<String, String> responseMap = new HashMap<>();

                            responseMap.put("RelationURI", reltioObject.uri);
                            responseMap.put("RelationType", relationName);
                            responseMap.put("StartObjectUri", reltioObject.startObject.objectURI);
                            responseMap.put("EndObjectUri", reltioObject.endObject.objectURI);

                            for (Map.Entry<String, InputAttribute> attr : attributes.entrySet()) {
                                List<Object> attributeData = reltioObject.attributes.get(attr.getKey());
                                ExtractServiceUtil.createExtractOutputData(attr, attributeData, responseMap, null);
                            }

                            finalResponseMap.add(responseMap);

                            List<String[]> finalResponse = new ArrayList<String[]>();
                            for (Map<String, String> resMap : finalResponseMap) {
                                finalResponse
                                        .add(objectArrayToStringArray(filterMapToObjectArray(resMap, responseHeader)));
                            }

                            writer.writeToFile(finalResponse);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                scanResponseObj.setObjects(null);
                initialjson = GSON.toJson(scanResponseObj.getCursor());
            }
        }

        Util.close(writer);

        System.out.println("Extract process Completed.....");
        long finalTime = System.currentTimeMillis() - programStartTime;
        System.out.println("[Performance]:  Total processing time : " + (finalTime / 1000) + "  Seconds");

    }

    public static Object[] filterMapToObjectArray(final Map<String, ?> values, final String[] nameMapping) {

        if (values == null) {
            throw new NullPointerException("values should not be null");
        } else if (nameMapping == null) {
            throw new NullPointerException("nameMapping should not be null");
        }

        final Object[] targetArray = new Object[nameMapping.length];
        int i = 0;
        for (final String name : nameMapping) {
            targetArray[i++] = values.get(name);
        }
        return targetArray;
    }

    public static String[] objectArrayToStringArray(final Object[] objectArray) {
        if (objectArray == null) {
            return null;
        }

        final String[] stringArray = new String[objectArray.length];
        for (int i = 0; i < objectArray.length; i++) {
            stringArray[i] = objectArray[i] != null ? objectArray[i].toString() : null;
        }

        return stringArray;
    }

}
