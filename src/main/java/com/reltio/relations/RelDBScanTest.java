package com.reltio.relations;

import com.google.gson.Gson;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.cst.util.Util;
import com.reltio.extract.domain.InputAttribute;
import com.reltio.extract.domain.ScanResponse;
import com.reltio.extract.util.ExtractServiceUtil;
import com.reltio.file.ReltioFileWriter;
import com.reltio.file.ReltioFlatFileWriter;
import com.reltio.util.PropertyUtil;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RelDBScanTest {

    public static final String SCAN_INPUT = "{\"type\":\"configuration/relationTypes/:RelationType\",\"pageSize\":MAX_PAGE_SIZE ,  \"timeLimit\":TIME_LIMIT}";
    private static final String[] DefaultAttributes = {"RelationURI", "RelationType", "StartObjectUri",
            "EndObjectUri"};
    private static final Gson GSON = new Gson();
    private static String currentCursorValue;
    private static String previousCursorValue;

    public static void main(String[] args) throws Exception {

        System.out.println("Process Started");

        if (args.length == 0) {
            System.out.println("Please pass file path as argument!!!");
            System.exit(0);
        }
        long programStartTime = System.currentTimeMillis();
        // Encrypting Password
        Properties properties = Util.getProperties(args[0], "PASSWORD", "CLIENT_CREDENTIALS");

        List<String> requiredKeys = Arrays.asList("AUTH_URL", "OUTPUT_FILE", "ENVIRONMENT_URL", "ATTRIBUTE_FILE",
                "TENANT_ID", "RELATION_TYPE");

        // Missing Keys

        List<String> missingKeys = Util.listMissingProperties(properties, requiredKeys,
                PropertyUtil.getMutualExclusiveProps());

        if (!Util.isEmpty(missingKeys)) {
            System.out.println("Properties are missing \n" + String.join("\n", missingKeys));
            System.exit(0);
        }

        String DATALOAD_SERVER_HOST = properties.getProperty("ENVIRONMENT_URL");
        String TENANT_ID = properties.getProperty("TENANT_ID");
        String OUTPUT_FILE_PATH = properties.getProperty("OUTPUT_FILE");
        String ATTRIBUTE_FILE_PATH = properties.getProperty("ATTRIBUTE_FILE");
        String RELATION_TYPE = properties.getProperty("RELATION_TYPE");
        String FILE_DELIMITER = properties.getProperty("FILE_DELIMITER", ",");
        String timeLimit = properties.getProperty("TIME_LIMIT", "1000");
        String recordsPerPost = properties.getProperty("RECORDS_PER_POST", "100");
        List<String> lookUpAttributes = Collections.emptyList();

        if (properties.getProperty("LOOKUP_REQUIRED", "No").equalsIgnoreCase("Yes")) {
            String lookupAttribute = properties.getProperty("LOOK_UP_ATTRIBUTES");
            lookUpAttributes = Stream.of(lookupAttribute.split(",")).collect(Collectors.toList());
        }

        // Proxy setup
        if (!Util.isEmpty(properties.getProperty("HTTP_PROXY_HOST"))
                && !Util.isEmpty(properties.getProperty("HTTP_PROXY_PORT"))) {
            Util.setHttpProxy(properties);
        }
        final ReltioAPIService reltioAPIService = Util.getReltioService(properties);
        Integer count = 0;

        final Map<String, InputAttribute> attributes = new LinkedHashMap<>();
        final ReltioFileWriter writer = new ReltioFlatFileWriter(OUTPUT_FILE_PATH, "UTF-8", FILE_DELIMITER);

        // Read OV Values Attribute
        if (ATTRIBUTE_FILE_PATH != null && !ATTRIBUTE_FILE_PATH.equalsIgnoreCase("null")) {
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(new FileInputStream(ATTRIBUTE_FILE_PATH), StandardCharsets.UTF_8));
            ExtractServiceUtil.createAttributeMapFromProperties(reader, attributes);
            Util.close(reader);
        }

        final List<String> fileResponseHeader = new ArrayList<>(Arrays.asList(DefaultAttributes));

        // Create Response file header
        for (Map.Entry<String, InputAttribute> attr : attributes.entrySet()) {
            ExtractServiceUtil.createExtractNestedResponseHeader(attr, fileResponseHeader, null, lookUpAttributes);
        }

        final String[] responseHeader = new String[fileResponseHeader.size()];
        fileResponseHeader.toArray(responseHeader);

        writer.writeToFile(responseHeader);

        final String scanUrl = "https://" + DATALOAD_SERVER_HOST + "/reltio/api/" + TENANT_ID + "/relations/_dbscan";

        System.out.println(scanUrl);

        String initialJSON = SCAN_INPUT.replaceAll(":RelationType", RELATION_TYPE).replaceAll("TIME_LIMIT", timeLimit)
                .replaceAll("MAX_PAGE_SIZE", recordsPerPost);

        while (true) {

            // Doing the DBScan API Call here
            String scanResponse = reltioAPIService.post(scanUrl, initialJSON);

            // Convert the string the java object
            ScanResponse scanResponseObj = GSON.fromJson(scanResponse, ScanResponse.class);
            currentCursorValue = scanResponseObj.getCursor().getCursor();

            if (scanResponseObj.getObjects() == null) {
                System.out.println("RelDBScanTest: Scan Response Object is null\n" + scanResponse);
            } else {
                System.out.println("RelDBScanTest: Scan Response Object is not null and Object Size = " + scanResponseObj.getObjects().size());
            }

            if (currentCursorValue.equals(previousCursorValue) && scanResponseObj.getObjects() == null) {
                System.out.println("Cursors are same");
                break;
            } else {
                previousCursorValue = currentCursorValue;
                initialJSON = GSON.toJson(scanResponseObj.getCursor());
                if (scanResponseObj.getObjects() != null) {
                    count += scanResponseObj.getObjects().size();
                    System.out.println("Scanned records count = " + count);
                }
            }
        }

        System.out.println("Extract process Completed.....");
        long finalTime = System.currentTimeMillis() - programStartTime;
        System.out.println("[Performance]:  Total processing time : " + (finalTime / 1000) + "  Seconds");
    }
}
