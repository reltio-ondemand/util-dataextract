package com.reltio.relations;

import com.google.gson.Gson;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.cst.util.Util;
import com.reltio.extract.ExtractConstants;
import com.reltio.extract.domain.ExtractProperties;
import com.reltio.extract.domain.InputAttribute;
import com.reltio.extract.domain.ReltioObject;
import com.reltio.extract.domain.ScanResponse;
import com.reltio.extract.util.ExtractServiceUtil;
import com.reltio.file.ReltioCSVFileWriter;
import com.reltio.file.ReltioFileWriter;
import com.reltio.file.ReltioFlatFileWriter;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.*;

@Deprecated
public class RelationDeltaExtractByDBScan {

    private static final Gson GSON = new Gson();
    public static final int MAX_PAGE_SIZE = 100;
    public static final String SCAN_INPUT = "{\"type\":\"configuration/relationTypes/:RelationType\",\"pageSize\":"
            + MAX_PAGE_SIZE + "}";
    private static final String[] DefaultAttributes = {"RelationURI", "RelationType", "StartObjectUri",
            "EndObjectUri"};

    public static void main(String[] args) throws Exception {

        System.out.println("Process Started");
        long programStartTime = System.currentTimeMillis();

        Properties properties = null;

        try {

            System.out.println("Please pass file path as argument!!!");
            System.exit(0);
            properties = Util.getProperties(args[0]);

        } catch (Exception e) {
            System.out.println("Failed reading properties File...");
        }

        // READ the Properties values
        final ExtractProperties extractProperties = new ExtractProperties(properties);

        final ReltioAPIService reltioAPIService = Util.getReltioService(properties);

        int threadsNumber = extractProperties.getThreadCount();
        int count = 0;

        final Map<String, InputAttribute> attributes = new LinkedHashMap<>();

        final ReltioFileWriter fileWriter;

        // Output File
        // check whether its CSV out FILE or Flat File
        if (extractProperties.getFileFormat().equalsIgnoreCase("CSV")) {
            fileWriter = new ReltioCSVFileWriter(extractProperties.getOutputFilePath());
        } else if (extractProperties.getFileDelimiter() != null) {
            // provided file Delimiter
            fileWriter = new ReltioFlatFileWriter(extractProperties.getOutputFilePath(), ExtractConstants.ENCODING,
                    extractProperties.getFileDelimiter());
        } else {
            // Default delimiter pipe
            fileWriter = new ReltioFlatFileWriter(extractProperties.getOutputFilePath());
        }

        // Read OV Values Attribute
        if (extractProperties.getAttrFilePath() != null
                && !extractProperties.getAttrFilePath().equalsIgnoreCase("null")) {
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(new FileInputStream(extractProperties.getAttrFilePath()), "UTF-8"));
            ExtractServiceUtil.createAttributeMapFromProperties(reader, attributes);

            Util.close(reader);
        }

        final List<String> fileResponseHeader = new ArrayList<>(Arrays.asList(DefaultAttributes));

        // Create Response file header
        for (Map.Entry<String, InputAttribute> attr : attributes.entrySet()) {
            ExtractServiceUtil.createExtractNestedResponseHeader(attr, fileResponseHeader, null, null);
        }

        final String[] responseHeader = new String[fileResponseHeader.size()];
        fileResponseHeader.toArray(responseHeader);

        if (extractProperties.getIsHeaderRequired() == null
                || extractProperties.getIsHeaderRequired().equalsIgnoreCase("Yes")) {
            fileWriter.writeToFile(responseHeader);
        }

        final String scanUrl = extractProperties.getApiUrl() + "/relations/_dbscan";
        System.out.println(scanUrl);
        
        String intitialJSON = SCAN_INPUT.replaceAll(":RelationType", extractProperties.getRelationType());

        ExecutorService executorService = Executors.newFixedThreadPool(threadsNumber);
        boolean eof = false;
        ArrayList<Future<Long>> futures = new ArrayList<Future<Long>>();

        while (!eof) {

            // Doing the DBScan API Call here
            String scanResponse = reltioAPIService.post(scanUrl, intitialJSON);

            // Convert the string the java object
            ScanResponse scanResponseObj = GSON.fromJson(scanResponse, ScanResponse.class);

            if (scanResponseObj.getObjects() != null && scanResponseObj.getObjects().size() > 0) {

                count += scanResponseObj.getObjects().size();
                System.out.println("Scanned records count = " + count);

                final List<ReltioObject> objectsToProcess = scanResponseObj.getObjects();

                int threadNum = 0;

                for (final ReltioObject reltioObject : objectsToProcess) {

                    threadNum++;

                    futures.add(executorService.submit(() -> {
                        long requestExecutionTime = 0L;
                        try {

                            Long updatedTime = Long.parseLong(reltioObject.updatedTime);
                            if (updatedTime > convertDateToLong(extractProperties.getFromDate())
                                    && updatedTime <= convertDateToLong(extractProperties.getToDate())) {

                                String relationName = reltioObject.type.replace("configuration/relationTypes/", "");

                                List<Map<String, String>> finalResponseMap = new ArrayList<>();

                                Map<String, String> responseMap = new HashMap<String, String>();
                                responseMap.put("RelationURI", reltioObject.uri);
                                responseMap.put("RelationType", relationName);
                                responseMap.put("StartObjectUri", reltioObject.startObject.objectURI);
                                responseMap.put("EndObjectUri", reltioObject.endObject.objectURI);

                                for (Map.Entry<String, InputAttribute> attr : attributes.entrySet()) {
                                    List<Object> attributeData = reltioObject.attributes.get(attr.getKey());
                                    ExtractServiceUtil.createExtractOutputData(attr, attributeData, responseMap,
                                            null);
                                }

                                finalResponseMap.add(responseMap);

                                List<String[]> finalResponse = new ArrayList<String[]>();
                                for (Map<String, String> resMap : finalResponseMap) {
                                    finalResponse.add(objectArrayToStringArray(
                                            filterMapToObjectArray(resMap, responseHeader)));
                                }

                                fileWriter.writeToFile(finalResponse);

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return requestExecutionTime;
                    }));

                    if (threadNum >= threadsNumber) {

                        threadNum = 0;
                        waitForTasksReady(futures, threadsNumber * 2, threadsNumber);

                    }

                }

            } else {
                eof = true;
                break;
            }
            scanResponseObj.setObjects(null);
            intitialJSON = GSON.toJson(scanResponseObj.getCursor());

        }

        waitForTasksReady(futures, 0, threadsNumber * 3);
        executorService.shutdown();

        Util.close(fileWriter);

        System.out.println("Extract process Completed.....");
        long finalTime = System.currentTimeMillis() - programStartTime;
        System.out.println("[Performance]:  Total processing time : " + (finalTime / 1000) + "  Seconds");

    }

    public static int waitForTasksReady(Collection<Future<Long>> futures, int maxNumberInList, int threadsNumber) {
        int totalResult = 0;
        if (futures.size() > maxNumberInList) {
            for (Future<Long> future : new ArrayList<>(futures)) {
                try {

                    future.get();
                    totalResult += 1;
                    futures.remove(future);

                    if (totalResult >= threadsNumber) {
                        break;
                    }

                } catch (Exception e) {
                    System.out.println("Failed to check thread completion " + e);
                }
            }
        }
        return totalResult;
    }

    public static Object[] filterMapToObjectArray(final Map<String, ?> values, final String[] nameMapping) {

        if (values == null) {
            throw new NullPointerException("values should not be null");
        } else if (nameMapping == null) {
            throw new NullPointerException("nameMapping should not be null");
        }

        final Object[] targetArray = new Object[nameMapping.length];
        int i = 0;
        for (final String name : nameMapping) {
            targetArray[i++] = values.get(name);
        }
        return targetArray;
    }

    public static String[] objectArrayToStringArray(final Object[] objectArray) {
        if (objectArray == null) {
            return null;
        }

        final String[] stringArray = new String[objectArray.length];
        for (int i = 0; i < objectArray.length; i++) {
            stringArray[i] = objectArray[i] != null ? objectArray[i].toString() : null;
        }

        return stringArray;
    }

    private static long convertDateToLong(String date) throws Exception {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateFormat.parse(date));
        // System.out.println(lDate);
        return cal.getTimeInMillis();
    }

}
