
#Data Extract Utility

## Description
The Data Extract tool is comprised of two different tools, the OV Extract and Xref Extract tools. The OV tool is used to extract the golden record, whereas the Xref tool extracts each record into multiple records based on the crosswalk that it was sourced from. The Data Extract tool is used to extract the values from a tenant into CSV or flatfile format for use in downstream processes and reporting. The tool supports simple, nested, and reference attributes for export and can handle multivalue fields. To create a new job with the Data Extract tool a configuration file and mapping file must be created to be used as parameters.

##Change Log


```
#!plaintext
Last Update Date    : 04/30/2021
Version             : 1.4.1
Last Update By      : Shivaputrappa Patil
Description         : Added UFT-8 format for FileReaderBuilder  https://reltio.jira.com/browse/ROCS-110

#!plaintext
Last Update Date    : 23/12/2020
Version             : 1.4.0
Last Update By      : Shivaputrappa Patil
Description         : Added look up code for attributes configured in proerties https://reltio.jira.com/browse/CUST-3166

#!plaintext
Last Update Date    : 21/12/2020
Version             : 1.3.9
Last Update By      : Shivaputrappa Patil
Description         : removed check for the exportUrl v2 export Extract ROCS-107

#!plaintext
Last Update Date    : 30/07/2020
Version             : 1.3.8
Last Update By      : Sanjay
Description         : Added sourceTable in Xref Extract
             
Last Update Date    : 01/04/2020
Version             : 1.3.6
Last Update By      : Sanketha CR
Description         : Added cursor and relation object codition checks, 
                      Removed multithreading for Relation DBScan.

#!plaintext
Last Update Date    : 07/01/2020
Version             : 1.3.3
Last Update By      : Shivaputrappa Patil
Description         : Added UTF-8 encoding for streaming reader

#!plaintext
Last Update Date    : 19 Sept 2019
Version             : 1.3.1
Last Update By      : Vignesh Chandran
Description         : Client Credentials Impl, V2 Support
                      Deprecated RelationDeltaExtractDBScan
                      Test Cases Impl
                      https://reltio.jira.com/browse/ROCS-69
                      https://reltio.jira.com/browse/ROCS-83


Last Update Date    : 02 July 2019
Version             : 1.3.0
Last Update By      : Vignesh Chandran
Description         : Proxy Support (https://reltio.jira.com/browse/ROCS-40)
                    : Standardization of Jar (https://reltio.jira.com/browse/ROCS-39)
                    : Folder Structure Changes.

Last Update Date: 25/01/2019
Version: 1.2.7
Description: Clear validation message, encryption of password

#!plaintext
Last Update Date: 26/12/2018
Version: 1.2.6
Description: Standarization of Logs to Log4j2,Remove unused dependency,Add OPS

Last Update Date: 21/11/2018
Version: 1.2.6
Description: Updated optional paramenters to scanning config
```
##Contributing 
Please visit our [Contributor Covenant Code of Conduct](https://bitbucket.org/reltio-ondemand/common/src/a8e997d2547bf4df9f69bf3e7f2fcefe28d7e551/CodeOfConduct.md?at=master&fileviewer=file-view-default) to learn more about our contribution guidlines

## Licensing
```
#!plaintext
Copyright (c) 2017 Reltio

 

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

 

    http://www.apache.org/licenses/LICENSE-2.0

 

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

See the License for the specific language governing permissions and limitations under the License.
```

## Quick Start 
To learn about dependencies, building and executing the tool view our [quick start](https://bitbucket.org/reltio-ondemand/util-dataextract/src/fe9424295fb3bfd4a70ae12ca7d21c4173cafdb1/QuickStart.md?at=master&fileviewer=file-view-default).


