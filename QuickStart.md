# Quick Start 

##Building

There are two versions of the utility, one to extract the OV Value, and the other to extract a particular crosswalks values. The build path for each is below.
1. OV Value Extract: **data-extract/src/main/java/com/reltio/extract/denormalized/service /AttributeExtractReport.java**
2. Crosswalk Value Extract: **data-extract/src/main/java/com/reltio/extract/denormalized/service /XrefExtractReport.java**

##Dependencies 

1. gson-2.2.4
2. reltio-cst-core-1.4.9



##Parameters File For Entities OV Extract

``` 
##Common Properties

#!paintext
ENVIRONMENT_URL= <Server Host URL>
TENANT_ID= <Tenant ID>
AUTH_URL= <Auth URL>
ENTITY_TYPE= <ENTITY Type to be extracted>
USERNAME= <Reltio UserName>
PASSWORD= <Reltio Password>
OV_ATTRIBUTE_FILE_LOCATION = <file path to attribite mapping
OUTPUT_FILE = <file path for results>
FILE_FORMAT= <CSV/FLAT>
FILE_DELIMITER= <If Flat, what delimiter>
HEADER_REQUIRED= <YES/NO, Do you want column header for results?>
THREAD_COUNT=< Number of Threads, recommended 10, max 30>
RECORDS_PER_POST=<<Specifies how many entities to return in each request. Default is 10.>>
TIME_LIMIT=<<Specifies the amount of time (in ms) before _dbscan returns a cursor if nothing or fewer entities than pageSize are found>>
EXTRACT_TYPE=v2export
#type of method to read from reltio available options are 

#  esexport, dbscan, esscan, cassandraexport and v2export. by default dbscan will be used if not specified this property
RECORDS_PER_POST=100

#if attributes look code needs to copied to extract file
LOOKUP_REQUIRED=Yes/No
#attribute list seperated by comma
LOOK_UP_ATTRIBUTES=AddressType,Type

```
Note on v2export (Smart Export) : This option is provided for supporting the v2 export type also know as smart export.

##Parameters File For Entities Crosswalk Extract

```
#!paintext
ENVIRONMENT_URL= <Server Host URL>
TENANT_ID= <Tenant ID>
AUTH_URL= <Auth URL>
ENTITY_TYPE= <ENTITY Type to be extracted>
USERNAME= <Reltio UserName>
PASSWORD= <Reltio Password>
XREF_ATTRIBUTE_FILE_LOCATION = <file path to attribite mapping
OUTPUT_FILE = <file path for results>
FILE_FORMAT= <CSV/FLAT>
FILE_DELIMITER= <If Flat, what delimiter>
HEADER_REQUIRED= <YES/NO, Do you want column header for results?>
THREAD_COUNT=< Number of Threads, recommended 10, max 30>
RECORDS_PER_POST=<<Specifies how many entities to return in each request. Default is 10.>>
TIME_LIMIT=<<Specifies the amount of time (in ms) before _dbscan returns a cursor if nothing or fewer entities than pageSize are found>>
#type of method to read from reltio available options are 

#if attributes look code needs to copied to extract file
LOOKUP_REQUIRED=Yes/No
#attribute list seperated by comma
LOOK_UP_ATTRIBUTES=AddressType,Type
#type of method to read from reltio available options are 

#EXTRACT_TYPE for relations we have only dbscan, we can't export the relation from any other extract type like esexport, esscan, cassandraexport and v2export.
EXTRACT_TYPE=v2export

```

##Parameters File for Relations

```
#!paintext
ENVIRONMENT_URL= <Server Host URL>  Eg:dev.reltio.com
TENANT_ID= <Tenant ID>
AUTH_URL= <Auth URL>   Eg:https://auth.reltio.com/oauth/token
USERNAME= <Reltio UserName>
PASSWORD= <Reltio Password>
RELATION_TYPE=<RELATION_NAME>  Eg:BRToBRI
ATTRIBUTE_FILE = <file path to attribite mapping  E:\\attr.txt
OUTPUT_FILE = <file path for results>  E:\\out.txt
THREAD_COUNT=< Number of Threads, recommended 10, max 30>
FILE_DELIMITER= <If Flat, what delimiter>
RECORDS_PER_POST=<<Specifies how many entities to return in each request. Default is 10.>>
TIME_LIMIT=<<Specifies the amount of time (in ms) before _dbscan returns a cursor if nothing or fewer entities than pageSize are found>>
EXTRACT_TYPE=esscan

#  esexport, dbscan, esscan, cassandraexport and v2export. by default dbscan will be used if not specified this property


```
##Tool Specific Properties
##Attribute File Example

```
#!paintext
FirstName
LastName
Phone.Number
Phone.Type
Address.ZipCode.Zip5


```

##Executing

Command to start the utility.
```
#!plaintext

java -jar reltio-util-data-extract-entity-xref-<<verion>>-jar-with-dependencies.jar propertiesFile.txt > $logfilepath$
java -jar reltio-util-data-extract-entity-ov--<<verion>>-jar-with-dependencies.jar propertiesFile.txt > $logfilepath$
java -jar reltio-util-data-extract-relation-st-<<verion>>-jar-with-dependencies.jar propertiesFile.txt > $logfilepath$
java -jar reltio-util-data-extract-relation-<<verion>>-jar-with-dependencies.jar propertiesFile.txt > $logfilepath$

```
